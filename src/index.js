import React from 'react';
import PropTypes from 'prop-types';
import { render, Artboard, Text, View, makeSymbol } from 'react-sketchapp';
import chroma from 'chroma-js';

// take a hex and give us a nice text color to put over it
const textColor = (hex) => {
  const vsWhite = chroma.contrast(hex, 'white');
  if (vsWhite > 4) {
    return '#FFF';
  }
  return chroma(hex)
    .darken(3)
    .hex();
};

const Swatch = ({ name, hex }) => (
  <View
    name={`${name}`}
    style={{
      height: 40,
      width: 40,
      backgroundColor: hex
    }}
  >   
  </View>
);

const Color = {
  hex: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

Swatch.propTypes = Color;

const Document = ({ colors }) => {
  const colorSymbols = Object.keys(colors).map(color => (
    makeSymbol(
      () => <Swatch name={color} hex={colors[color]} />,
      color
    )
  ));

  return (<Artboard
      name="Colors"
      style={{
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: (96 + 8) * 4,
      }}
    >
      {colorSymbols.map(Symbol => <Symbol />)}
    </Artboard>
  )
};

Document.propTypes = {
  colors: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default () => {
  const colorList = {
    R50:'#FFEBE6',
    R75:'#FFBDAD',
    R100:'#FF8F73',
    R200:'#FF7452',
    R300:'#FF5630',
    R400:'#DE350B',
    R500:'#BF2600'
  };

  render(<Document colors={colorList} />, context.document.currentPage());
};
